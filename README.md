# Composite launch configuration plugin for Eclipse IDE #

The plugin allows to launch a set of configurations.

### Installation

1. Download plugin archive [here](https://bitbucket.org/mvpotter/composite-launch-eclipse-plugin/downloads/composite-launch-plugin-1.0.0.zip).
2. Go to: Help -> Install New Software... -> Add... -> Archive...
3. Choose "composite-launch-plugin-1.0.0.zip" and click "Ok" button
4. Ensure that "Group items by category" checkbox is checked off
5. Check "Composite launch plugin" and click "Finish" button.