package com.mvpotter.composite.launch.core.i18n;

import org.eclipse.osgi.util.NLS;

public class Messages {
	
	private static final String MESSAGES_BASE_NAME = "com.mvpotter.composite.launch.core.i18n.messages"; 
	
	public static String errorInvalidConfiguration;
	public static String errorInvalidLaunchMode;
	
	static {
		NLS.initializeMessages(MESSAGES_BASE_NAME, Messages.class);
	}
	
}
