package com.mvpotter.composite.launch.core.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;

public class Configuration {

	public static final String GROUP_TYPE = "com.mvpotter.composite.launch";
	public static final String CONFIGURATIONS_ATTRIBUTE = "configurations";
	
	public static boolean isComposite(final ILaunchConfiguration configuration) throws CoreException {
		return GROUP_TYPE.equals(configuration.getType().getIdentifier());
	}
	
	public static boolean hasCyclicDependencies(final ILaunchConfiguration configuration) throws CoreException {
		
		final List<ILaunchConfiguration> visited = new LinkedList<>();
		final Stack<ILaunchConfiguration> stack = new Stack<>();
		stack.add(configuration);
		
		while (!stack.isEmpty()) {
			final ILaunchConfiguration node = stack.pop();
			if (isComposite(node)) {
				visited.add(node);
				final List<ILaunchConfiguration> children = findChildren(node);
				for (final ILaunchConfiguration child: children) {
					if (isComposite(child)) {
						if (visited.contains(child)) {
							return true;
						} else {
							stack.push(child);
						}
					}
				}
			}
		}

		return false;
	}
	
	public static List<ILaunchConfiguration> findChildren(final ILaunchConfiguration configuration) {
		try {
			final List<String> configurations = configuration.getAttribute(CONFIGURATIONS_ATTRIBUTE, Collections.emptyList());
			final List<ILaunchConfiguration> result = new ArrayList<ILaunchConfiguration>();
			for (final String name : configurations) {
				final ILaunchConfiguration foundConfiguration = findByName(name);
				if (foundConfiguration != null) {
					result.add(foundConfiguration);
				}
			}
			
			return result;
		} catch (final CoreException ignore) {
			return Collections.emptyList();
		}
	}
	
	public static ILaunchConfiguration findByName(final String name) {
		if (name != null) {
			for (final ILaunchConfiguration configuration : findAll()) {
				if (name.equals(configuration.getName())) {
					return configuration;
				}
			}
		}
		
		return null;
	}
	
	public static ILaunchConfiguration findByName(final String name, final List<ILaunchConfiguration> configurations) {
		for (final ILaunchConfiguration configuration: configurations) {
			if (configuration.getName().equals(name)) {
				return configuration;
			}
		}
		
		return null;
	}
	
	public static List<ILaunchConfiguration> findAll() {
		return findAll(null);
	}
	
	public static List<ILaunchConfiguration> findAll(final String mode) {
		final List<ILaunchConfiguration> configurations = new ArrayList<ILaunchConfiguration>();
		try {
			for (final ILaunchConfiguration configuration : DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations()) {
				if (mode == null || configuration.supportsMode(mode)) {
					configurations.add(configuration);
				}
			}
		} catch(CoreException e) {
			return Collections.emptyList();
		}
		
		return configurations;
	}
	
}
