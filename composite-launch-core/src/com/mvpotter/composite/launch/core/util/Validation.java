package com.mvpotter.composite.launch.core.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;

public class Validation {
	
	public static boolean validateLaunchMode(final String mode) throws CoreException {
		return ILaunchManager.RUN_MODE.equalsIgnoreCase(mode) || ILaunchManager.DEBUG_MODE.equalsIgnoreCase(mode);
	}
	
	public static boolean validateConfiguration(final ILaunchConfiguration configuration) throws CoreException {
		return Configuration.isComposite(configuration) && !Configuration.hasCyclicDependencies(configuration);
	}
	
}
