package com.mvpotter.composite.launch.core.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;

import com.mvpotter.composite.launch.core.Activator;

public class Exceptions {

	public static CoreException createError(final String messageFormat, final Object... args) {
		return new CoreException(new Status(IStatus.ERROR, Activator.getPluginId(), NLS.bind(messageFormat, args)));
	}
	
}
