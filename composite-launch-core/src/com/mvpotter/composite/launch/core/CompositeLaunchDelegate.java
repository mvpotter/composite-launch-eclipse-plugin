package com.mvpotter.composite.launch.core;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.debug.core.model.IProcess;

import com.mvpotter.composite.launch.core.i18n.Messages;
import com.mvpotter.composite.launch.core.util.Configuration;
import com.mvpotter.composite.launch.core.util.Exceptions;
import com.mvpotter.composite.launch.core.util.Validation;

public class CompositeLaunchDelegate implements ILaunchConfigurationDelegate {

	@Override
	public void launch(final ILaunchConfiguration configuration, final String mode,
			final ILaunch launch, final IProgressMonitor monitor) throws CoreException {
		if (!Validation.validateLaunchMode(mode)) {
            throw Exceptions.createError(Messages.errorInvalidLaunchMode, mode);
		}
        if (!Validation.validateConfiguration(configuration)) {
            throw Exceptions.createError(Messages.errorInvalidConfiguration, configuration);
        }
        try {
        	final List<ILaunchConfiguration> configurations = Configuration.findChildren(configuration);
        	final SubMonitor subMonitor = SubMonitor.convert(monitor, configuration.getName(), configurations.size());
            for (final ILaunchConfiguration launchConfiguration: configurations) {
                if (!monitor.isCanceled()) {
                    launchInnerConfiguration(launchConfiguration, mode, launch, subMonitor.newChild(1));
                }
            }
        } finally {
            monitor.done();
        }
	}

	private void launchInnerConfiguration(final ILaunchConfiguration configuration, final String mode, final ILaunch launch, final IProgressMonitor monitor) throws CoreException {
		final ILaunch result = configuration.launch(mode, monitor);
		for (final IDebugTarget target : result.getDebugTargets()) {
			launch.addDebugTarget(target);
		}
		for (final IProcess process : result.getProcesses()) {
			launch.addProcess(process);
		}
	}
	
}
