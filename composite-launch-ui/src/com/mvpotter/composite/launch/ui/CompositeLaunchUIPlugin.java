package com.mvpotter.composite.launch.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class CompositeLaunchUIPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "com.mvpotter.launch.group"; //$NON-NLS-1$

	// The shared instance
	private static CompositeLaunchUIPlugin plugin;
	
	/**
	 * The constructor
	 */
	public CompositeLaunchUIPlugin() {
	}

	public void start(final BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	public void stop(final BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static CompositeLaunchUIPlugin getDefault() {
		return plugin;
	}

}
