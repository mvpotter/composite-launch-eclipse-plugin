package com.mvpotter.composite.launch.ui.i18n;

import org.eclipse.osgi.util.NLS;

public class Messages {
	
	private static final String MESSAGES_BASE_NAME = "com.mvpotter.composite.launch.ui.i18n.messages"; 
	
	public static String tabName;
	
	public static String btnUp;
	public static String btnAdd;
	public static String btnRemove;
	public static String btnDown;
	
	static {
		NLS.initializeMessages(MESSAGES_BASE_NAME, Messages.class);
	}
	
}
