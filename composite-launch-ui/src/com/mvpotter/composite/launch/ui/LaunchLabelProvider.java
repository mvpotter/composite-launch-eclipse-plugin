package com.mvpotter.composite.launch.ui;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.swt.graphics.Image;

public class LaunchLabelProvider extends org.eclipse.jface.viewers.LabelProvider {

	@Override
	public String getText(final Object element) {
		return ((ILaunchConfiguration) element).getName();
	}
	
	@Override
	public Image getImage(final Object element) {
		try {
			return DebugUITools.getImage(((ILaunchConfiguration) element).getType().getIdentifier());
		} catch (final CoreException ignore) {
			return null;
		}
	}
	
}
