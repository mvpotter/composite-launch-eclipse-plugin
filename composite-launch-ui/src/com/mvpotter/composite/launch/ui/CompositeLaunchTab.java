package com.mvpotter.composite.launch.ui;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.mvpotter.composite.launch.core.util.Configuration;
import com.mvpotter.composite.launch.ui.i18n.Messages;

public class CompositeLaunchTab extends AbstractLaunchConfigurationTab {

	private final List<ILaunchConfiguration> allConfigurations = new ArrayList<>();
	private final List<ILaunchConfiguration> selectedConfigurations = new ArrayList<>();
	private final String mode;
	
	private TableViewer allConfigurationsViewer;
	private TableViewer selectedConfigurationsViewer;
	
	public CompositeLaunchTab(final String mode) {
		this.mode = mode;
	}
	
	@Override
	public void createControl(final Composite parent) {
		final Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(3, true));
		container.setFont(parent.getFont());
		allConfigurationsViewer = createConfigurationsViewer(container, allConfigurations);
		createButtonsBar(container);
		selectedConfigurationsViewer = createConfigurationsViewer(container, selectedConfigurations);
		setControl(container);
	}

	@Override
	public void setDefaults(final ILaunchConfigurationWorkingCopy configuration) {
		updateChildConfigurations(configuration, Collections.emptyList());
	}

	@Override
	public void initializeFrom(final ILaunchConfiguration configuration) {
		allConfigurations.clear();
		selectedConfigurations.clear();
		List<ILaunchConfiguration> selected = Configuration.findChildren(configuration);
		allConfigurations.addAll(Configuration.findAll(mode));
		// remove already selected configurations
		allConfigurations.removeAll(selected);
		// remove current composite configuration
		final ILaunchConfiguration currentConfiguration = Configuration.findByName(configuration.getName(), allConfigurations);
		allConfigurations.remove(currentConfiguration);
		selectedConfigurations.addAll(selected);
		notifyContentChanged();
	}

	@Override
	public void performApply(final ILaunchConfigurationWorkingCopy configuration) {
		updateChildConfigurations(configuration, getNames(selectedConfigurations));
	}

	@Override
	public String getName() {
		return Messages.tabName;
	}
	
	private TableViewer createConfigurationsViewer(final Composite parent, final List<ILaunchConfiguration> input) {
		final GridData data = new GridData();
		
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = true;
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;

		final TableViewer viewer = new TableViewer(parent);
		viewer.getControl().setLayoutData(data);
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setLabelProvider(new LaunchLabelProvider());
		viewer.setInput(input);
		
		return viewer;
	}
	
	private void createButtonsBar(final Composite parent) {
		final Composite buttonsLayout = createButtonsLayout(parent);
		
		final Button moveDown = createPushButton(buttonsLayout, Messages.btnDown, null);
		final Button add = createPushButton(buttonsLayout, Messages.btnAdd, null);
		final Button remove = createPushButton(buttonsLayout, Messages.btnRemove, null);
		final Button moveUp = createPushButton(buttonsLayout, Messages.btnUp, null);
		
		add.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent event) {
				addConfiguration();
			}
		});
		
		remove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent event) {
				removeConfigurations();
			}
		});
		
		moveUp.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent event) {
				moveSelectedConfiguration(-1);
			}
		});
		
		moveDown.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent event) {
				moveSelectedConfiguration(1);
			}
		});
	}
	
	private Composite createButtonsLayout(final Composite parent) {
		final Composite composite = new Composite(parent, SWT.NONE);
		final GridData data = new GridData(GridData.HORIZONTAL_ALIGN_CENTER);
    	
		data.verticalSpan = 5;
		composite.setLayout(new GridLayout(1, false));
		composite.setFont(parent.getFont());
		composite.setLayoutData(data);
    	
		return composite;
	}
	
	private void addConfiguration() {
		moveSelectedConfigurations(allConfigurationsViewer, selectedConfigurationsViewer);
	}

	private void removeConfigurations() {
		moveSelectedConfigurations(selectedConfigurationsViewer, allConfigurationsViewer);
	}
	
	@SuppressWarnings("unchecked")
	private void moveSelectedConfigurations(final TableViewer source, final TableViewer dest) {
		List<ILaunchConfiguration> configurations = ((IStructuredSelection) source.getSelection()).toList();
		((List<ILaunchConfiguration>) source.getInput()).removeAll(configurations);
		((List<ILaunchConfiguration>) dest.getInput()).addAll(configurations);
		notifyContentChanged();
	}
	
	private void moveSelectedConfiguration(final int shift) {
		final IStructuredSelection selection = (IStructuredSelection) selectedConfigurationsViewer.getSelection();
		final ILaunchConfiguration selected = (ILaunchConfiguration) selection.getFirstElement();

		if (selected != null && selection.toArray().length == 1) {
			final int oldIndex = selectedConfigurations.indexOf(selected);
			final int newIndex = Math.max(0, Math.min(oldIndex + shift, selectedConfigurations.size() - 1));
			selectedConfigurations.set(oldIndex, selectedConfigurations.get(newIndex));
			selectedConfigurations.set(newIndex, selected);
			notifyContentChanged();
		}
	}
	
	private void notifyContentChanged() {
		updateLaunchConfigurationDialog();
		allConfigurationsViewer.refresh();
		selectedConfigurationsViewer.refresh();
	}
	
	private void updateChildConfigurations(final ILaunchConfigurationWorkingCopy configuration, final List<String> configurations) {
		configuration.setAttribute(Configuration.CONFIGURATIONS_ATTRIBUTE, configurations);
	}
	
	private List<String> getNames(final List<ILaunchConfiguration> configurations) {
		final List<String> names = new ArrayList<>(configurations.size());
		
		for (final ILaunchConfiguration configuration : configurations) {
			names.add(configuration.getName());
		}
		
		return names;
	}
	
}
